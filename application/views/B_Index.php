<!DOCTYPE html>
<html>
<head>
	<title>Mata Pelajaran</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
	<script type="text/javascript" src="<?php echo base_url('assets/jquery/jquery-3.3.1.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js'); ?>"></script>
</head>
<body style="margin: 20px;">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<b class="col-md-10">Masukan Data Pelajaran</b>
			<center><button data-toggle="modal" data-target="#addModal" class="btn btn-success">Tambah Data</button></center>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Pelajaran</th>
							<th>Mata Pelajaran</th>
							<th>Nama Pengarang</th>
							<th>Kelas</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbl_data">
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>

	<!-- Modal Tambah-->
	<div id="addModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Tambah Data Pelajaran</h4>
	      </div>
	      <div class="modal-body">
	        <form>
	        	<div class="form-group">
	        		<label for="kodepelajaran">Kode Pelajaran</label>
	        		<input type="text" name="kodepelajaran" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="matapelajaran">Mata Pelajaran</label>
	        		<input type="text" name="matapelajaran" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="namapengarang">Nama Pengarang</label>
	        		<input type="text" name="namapengarang" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="kelas">kelas</label>
	        		<input type="text" name="kelas" class="form-control"></input>
	        	</div>

	        </form>
	      </div>
	      <div class="modal-footer">
	       <button type="button" class="btn btn-success" id="btn_add_data">Simpan</button>
	       <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

	<!-- Modal Edit-->
	<div id="editModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Edit Data</h4>
	      </div>
	      <div class="modal-body">
	        <form>
	        	<div class="form-group">
	        		<label for="kodepelajaran">Kode Pelajaran</label>
	        		<input type="text" name="kodepelajaran_edit" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="matapelajaran">Mata Pelajaran</label>
	        		<input type="text" name="matapelajaran_edit" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="namapengarang">Nama Pengarang</label>
	        		<input type="text" name="namapengarang_edit" class="form-control"></input>
	        	</div>
	        	<div class="form-group">
	        		<label for="kelas">kelas</label>
	        		<input type="text" name="kelas_edit" class="form-control"></input>
	        	</div>

	        </form>
	      </div>
	      <div class="modal-footer">
	       <button type="button" class="btn btn-danger" id="btn_update_data">Update</button>
	       <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
	      </div>
	    </div>

  
	  </div>
	</div>
<button type="button" class="btn btn-default" onclick="goBack()">Go Back </button>
<script> 
	function goBack(){
		window.history.back();
	}
</script>
	
</button>
</html>
<script type="text/javascript">
	$(document).ready(function(){
		tampil_data();
		//Menampilkan Data di tabel
		function tampil_data(){
			$.ajax({
				url: '<?php echo base_url(); ?>D_Index/ambilData',
				type: 'POST',
				dataType: 'json',
				success: function(response){
					console.log(response);
					var i;
					var no = 0;
					var html = "";
					for(i=0;i < response.length ; i++){
						no++;
						html = html + '<tr>'
									+ '<td>' + no  + '</td>'
									+ '<td>' + response[i].kodepelajaran + '</td>'
									+ '<td>' + response[i].matapelajaran + '</td>'
									+ '<td>' + response[i].namapengarang + '</td>'
									+ '<td>' + response[i].kelas  + '</td>'
									+ '<td style="width: 16.66%;">' + '<span><button data-id="'+response[i].kodepelajaran+'" class="btn btn-primary btn_edit">Edit</button><button style="margin-left: 5px;" data-id="'+response[i].kodepelajaran+'" class="btn btn-danger btn_hapus">Hapus</button></span>'  + '</td>'
									+ '</tr>';
					}
					$("#tbl_data").html(html);
				}

			});
		}
		//Hapus Data dengan konfirmasi
		$("#tbl_data").on('click','.btn_hapus',function(){
			var kodepelajaran = $(this).attr('data-id');
			var status = confirm('Yakin ingin menghapus?');
			if(status){
				$.ajax({
					url: '<?php echo base_url(); ?>D_Index/hapusData',
					type: 'POST',
					data: {kodepelajaran:kodepelajaran},
					success: function(response){
						tampil_data();
					}
				})
			}
		})
		//Menambahkan Data ke database
		$("#btn_add_data").on('click',function(){
			var kodepelajaran= $('input[name="kodepelajaran"]').val();
			var matapelajaran= $('input[name="matapelajaran"]').val();
			var namapengarang = $('input[name="namapengarang"]').val();
			var kelas = $('input[name="kelas"]').val();
			$.ajax({
				url: '<?php echo base_url(); ?>D_Index/tambahData',
				type: 'POST',
				data: {kodepelajaran:kodepelajaran,matapelajaran:matapelajaran,namapengaran g:namapengarang,kelas:kelas},
				success: function(response){
					$('input[name="kodepelajaran"]').val("");
					$('input[name="matapelajaran"]').val("");
					$('input[name="namapengarang"]').val("");
					$('input[name="kelas"]').val("");
					$("#addModal").modal('hide');
					tampil_data();
				}
			})

		});
		//Memunculkan modal edit
		$("#tbl_data").on('click','.btn_edit',function(){
			var kodepelajaran = $(this).attr('data-id');
			$.ajax({
				url: '<?php echo base_url(); ?>D_Index/getDataByKodepelajaran',
				type: 'POST',
				data: {kodepelajaran:kodepelajaran},
				dataType: 'json',
				success: function(response){
					console.log(response);
					$("#editModal").modal('show');
					$('input[name="kodepelajaran_edit"]').val(response[0].kodepelajaran);
					$('input[name="matapelajaran_edit"]').val(response[0].matapelajaran);
					$('input[name="namapengarang_edit"]').val(response[0].namapengarang);
					$('input[name="kelas_edit"]').val(response[0].kelas);
				}
			})
		});

		//Meng-Update Data
		$("#btn_update_data").on('click',function(){
			var kodepelajaran = $('input[name="kodepelajaran_edit"]').val();
			var matapelajaran = $('input[name="matapelajaran_edit"]').val();
			var namapengarang = $('input[name="namapengarang_edit"]').val();
			var kelas = $('input[name="kelas_edit"]').val();
			$.ajax({
				url: '<?php echo base_url(); ?>D_Index/perbaruiData',
				type: 'POST',
				data: {kodepelajaran:kodepelajaran,matapelajaran:matapelajaran,namapengarang:namapengarang,kelas:kelas},
				success: function(response){
					$('input[name="kodepelajaran_edit"]').val("");
					$('input[name="matapelajaran_edit"]').val("");
					$('input[name="namapengarang_edit"]').val("");
					$('input[name="kelas_edit"]').val("");
					$("#editModal").modal('hide');
					tampil_data();
				}
			})

		});
	});
</script>